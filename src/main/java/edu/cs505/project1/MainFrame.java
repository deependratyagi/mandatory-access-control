package edu.cs505.project1;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Logger log;
	private PropertyFileReader reader;
	private final String [] ACCESSLEVEL = {"General","Engg.","HR","Finance","HE","HF","FE","L"};

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		// Initialize Logs
		try{
		log = Logger.getLogger("Dblog");
		FileHandler fh = new FileHandler("C:/Users/dty222/dblog.log");
		log.addHandler(fh);
		log.setLevel(Level.ALL);
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);
		log.info("Application Started.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		setTitle("Mandatory Access Control Utility");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 400);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		
		setContentPane(contentPane);
		
		JLabel userLabel = new JLabel("User:");
		userLabel.setBounds(1, 10, 30, 25);
		contentPane.add(userLabel);
		
		JTextField userText = new JTextField();
		userText.setBounds(50, 10, 60, 25);
		contentPane.add(userText);
		
		JLabel passLabel = new JLabel("Password:");
		passLabel.setBounds(125, 10, 100, 25);
		contentPane.add(passLabel);
		
		JPasswordField passText = new JPasswordField();
		passText.setBounds(200, 10, 60, 25);
		contentPane.add(passText);
		
		JButton loginButton = new JButton("Login");
		loginButton.setBounds(280,10,100,25);
		contentPane.add(loginButton);
		
		loginButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				// read the file and check if username password is correct
				reader = new PropertyFileReader(log);
				Map<String, String> map = reader.read("/login.properties");
				
				String username = userText.getText();
				char[] password = passText.getPassword();
				String pass = "";
				for(char c : password){
					pass+=c;}
				
				if(map.containsKey(username) && map.get(username).equalsIgnoreCase(pass))
				{
					log.log(Level.INFO, "Login successful for user:"+username);
					// check if user is admin
					
					map = reader.read("/userinfo.properties");
					String accessLevel = map.get(username);
					if(map.containsKey(username) && accessLevel.equalsIgnoreCase("admin"))
					{
						// admin
						log.log(Level.INFO, "Admin Access to Utility for user:"+username);
						createAdminFrame(contentPane,map);
					}
					else{
						// normal user
						log.log(Level.INFO, "User Access to Utility for user:"+username);
						createUserFrame(contentPane,map);
					}
				}
				else{
					log.log(Level.WARNING, "Login failed for user:"+username);
					JOptionPane.showMessageDialog(contentPane, "Invalid Username or Password.");
					userText.setText("");
					passText.setText("");
				}
			}
		});
		
	}
	
	public void createAdminFrame(JPanel panel,Map<String, String> usermap)
	{
		
		
		panel.removeAll();
		panel.setLayout(null);
		
		setContentPane(panel);
		
		// divide the panel with vertical line 
		JSeparator vLine = new JSeparator(SwingConstants.VERTICAL);
		vLine.setBounds(270,0,10, this.getHeight());
		panel.add(vLine);
		
		// label for username
		JLabel newUserName = new JLabel("Enter Name");
		newUserName.setBounds(300, 10, 100, 25);
		panel.add(newUserName);
		
		// Input box for user name
		JTextField inputUserName = new JTextField();
		inputUserName.setBounds(400, 10, 100, 25);
		panel.add(inputUserName);
		
		// Label for Combo
		JLabel accessLabel = new JLabel("Select Level");
		accessLabel.setBounds(300, 50, 100, 25);
		panel.add(accessLabel);
		
		// Create drop down for access level
		JComboBox<String> levelCombo = new JComboBox<String>();
		levelCombo.setBounds(400, 50, 100, 25);
		for(String e : ACCESSLEVEL)
		{
			levelCombo.addItem(e);
			
		}
		levelCombo.setEditable(false);
		panel.add(levelCombo);
		
		
		// Label for selected access level
		JLabel selectedAccess = new JLabel("Selection");
		selectedAccess.setBounds(300, 90, 100, 25);
		panel.add(selectedAccess);
		
		// Show Selection
		final JTextField selectedLevel = new JTextField();
		selectedLevel.setBounds(400, 90, 100, 25);
		selectedLevel.setText("        ");
		selectedLevel.setEditable(false);
		panel.add(selectedLevel);
		
		// Event when access level is selected
		levelCombo.addActionListener(new ActionListener() {
					
			@Override
			public void actionPerformed(ActionEvent e) {
						
				JComboBox<String> comboBox = (JComboBox<String>) e.getSource();

		        Object selected = comboBox.getSelectedItem();
		        selectedLevel.setText(selected.toString());
					}
				});
		
		JButton newUser = new JButton("Create New User");
		newUser.setBounds(400, 140, 140, 25);
		panel.add(newUser);
		newUser.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
				/*if(inputUserName.getText()==null || newUserName.getText().equals(""))
				{
					JOptionPane.showMessageDialog(contentPane, "User name can not be empty.");
					return;
				}
				else if(selectedLevel.getText()==null || selectedLevel.getText().equals(""))
				{
					JOptionPane.showMessageDialog(contentPane, "Access level can not be empty.");
					return;
				}*/
				if(inputUserName.getText()!=null && selectedLevel.getText()!=null){
					String userName = inputUserName.getText();
					String accessCode = getAccessLevel(selectedLevel.getText());
					usermap.put(userName, accessCode);
					reader.write("/userinfo.properties", usermap);
				}
				else{
					JOptionPane.showMessageDialog(contentPane, "Username or Access level can not be empty.");
					return;
				}
			}
		});
		
		// Existing User and their Roles
		JLabel dropDownText = new JLabel("Select User");
		dropDownText.setBounds(1, 10, 80, 25);
		panel.add(dropDownText);
		
		JLabel selectionText = new JLabel("Selected User");
		selectionText.setBounds(1,50,120,25);
		panel.add(selectionText);
		
		final JTextField selectedUser = new JTextField();
		selectedUser.setBounds(85, 50, 100, 25);
		selectedUser.setText("        ");
		selectedUser.setEditable(false);
		panel.add(selectedUser);
		
		JComboBox<String> combo = new JComboBox<String>();
		combo.setBounds(75, 10, 100, 25);
		for(Entry<String, String> e : usermap.entrySet())
		{
			combo.addItem(e.getKey());
			
		}
		combo.setEditable(false);
		
		
		combo.addActionListener(new ActionListener() {
		      public void actionPerformed(ActionEvent e) {
		        
		    	    JComboBox<String> comboBox = (JComboBox<String>) e.getSource();

	                Object selected = comboBox.getSelectedItem();
	                selectedUser.setText(selected.toString());
	                // check the appropriate access level
	                String level = usermap.get(selected.toString());
	                CheckBox(panel,level);
		      }
		    });
		
		
		panel.add(combo);
		
		
		// create checkboxes
		JLabel general = new JLabel("General");
		general.setBounds(1,100,70,25);
		panel.add(general);
		
		JCheckBox gLevel = new JCheckBox("g");
		gLevel.setName("g");
		gLevel.setBounds(75, 100, 20, 25);
		gLevel.setSelected(false);
		panel.add(gLevel);
		
		JLabel e = new JLabel("Engg.");
		e.setBounds(1,130,40,25);
		panel.add(e);
		
		JCheckBox eLevel = new JCheckBox("e");
		eLevel.setName("e");
		eLevel.setBounds(75, 130, 20, 25);
		gLevel.setSelected(false);
		panel.add(eLevel);
		
		JLabel hr = new JLabel("HR");
		hr.setBounds(1,160,20,25);
		panel.add(hr);
		
		JCheckBox hrLevel = new JCheckBox("h");
		hrLevel.setName("h");
		hrLevel.setBounds(75, 160, 20, 25);
		hrLevel.setSelected(false);
		panel.add(hrLevel);
		
		JLabel finance = new JLabel("Finance");
		finance.setBounds(1,190,70,25);
		panel.add(finance);
		
		JCheckBox fLevel = new JCheckBox("f");
		fLevel.setName("f");
		fLevel.setBounds(75, 190, 20, 25);
		fLevel.setSelected(false);
		panel.add(fLevel);
		
		JLabel hf = new JLabel("HF");
		hf.setBounds(150,100,20,25);
		panel.add(hf);
		
		JCheckBox hfLevel = new JCheckBox("hf");
		hfLevel.setName("hf");
		hfLevel.setBounds(220, 100, 20, 25);
		hfLevel.setSelected(false);
		panel.add(hfLevel);
		
		JLabel he = new JLabel("HE");
		he.setBounds(150,130,20,25);
		panel.add(he);
		
		JCheckBox heLevel = new JCheckBox("he");
		heLevel.setName("he");
		heLevel.setBounds(220, 130, 20, 25);
		heLevel.setSelected(false);
		panel.add(heLevel);
		
		JLabel fe = new JLabel("FE");
		fe.setBounds(150,160,20,25);
		panel.add(fe);
		
		JCheckBox feLevel = new JCheckBox("fe");
		feLevel.setName("fe");
		feLevel.setBounds(220, 160, 20, 25);
		feLevel.setSelected(false);
		panel.add(feLevel);
		
		JLabel l = new JLabel("L");
		l.setBounds(150,190,20,25);
		panel.add(l);
		
		JCheckBox lLevel = new JCheckBox("l");
		lLevel.setName("l");
		lLevel.setBounds(220, 190, 20, 25);
		lLevel.setSelected(false);
		panel.add(lLevel);
		
		JButton saveButton = new JButton("Save");
		saveButton.setBounds(155,220,80,25);
		panel.add(saveButton);
		
		saveButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e)
			{
				String accessLevel = getSelectedCheck(panel);
				String username = selectedUser.getText();
				usermap.put(username, accessLevel);
				// save the usermap
				reader.write("/userinfo.properties", usermap);
			}
		});
		
		JButton logout = new JButton("");
	}
	
	public void createUserFrame(JPanel panel,Map<String, String> usermap)
	{
		panel.removeAll();
		
		panel.setLayout(null);
		
		setContentPane(panel);
	}
	
	private void CheckBox(JPanel panel,String accessLevel)
	{
		Component component[] = panel.getComponents();
		for(Component c : component)
		{
			System.out.println(c.getName());
			if(c.getName()!=null)
			{
				JCheckBox check = (JCheckBox)c;
				check.setSelected(false);
				if(c.getName().equalsIgnoreCase(accessLevel))
				{
					check.setSelected(true);
				}
			}
		}
	}
	
	private String getSelectedCheck(JPanel panel)
	{
		String checkName = null;
		Component component[] = panel.getComponents();
		for(Component c : component)
		{
			
			if((c.getName()!=null) && (c instanceof JCheckBox))
			{
				JCheckBox check = (JCheckBox)c;
				if(check.isSelected()){
				checkName = check.getName();
				}
			}
		}
		
		return checkName;
	}
	
	private String getAccessLevel(String level)
	{
		String accessCode = null;
		
		switch (level) {
		case "General":
			accessCode = "g";
			break;
		case "Engg.":
			accessCode = "e";
			break;
		case "HR":
			accessCode = "h";
		    break;
		case "Finance":
			accessCode = "f";
			break;
		case "HE":
			accessCode = "he";
			break;
		case "HF":
			accessCode = "hf";
		    break;
		case "FE":
			accessCode = "fe";
			break;
		case "L":
			accessCode = "l";
		    break;
		default:
			break;
		}
		
		return accessCode;
	}

}
