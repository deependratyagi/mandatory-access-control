package edu.cs505.project1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Logger;

public class PropertyFileReader {

	public PropertyFileReader(Logger log) {
		this.log = log;
	}

	
	private Logger log;

	public Map<String, String> read(String propertyFileName) {
		Map<String, String> map = new HashMap<>();
		try {
			Properties prop = new Properties();
			InputStream in = this.getClass().getResourceAsStream(propertyFileName);

			prop.load(in);
			in.close();
			for (final String name : prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return map;
	}

	public void write(String propertyFileName, Map<String, String> property) {

		FileOutputStream fos = null;
		try {
			File propFile = new File(this.getClass().getResource(propertyFileName).getPath());
			if (propFile.isFile()) {
				fos = new FileOutputStream(propFile);

				if (property != null && property.size() > 0) {
					Properties prop = new Properties();
					for (Entry<String, String> e : property.entrySet()) {
						prop.setProperty(e.getKey(), e.getValue());
					}
					prop.store(fos, null);
					
					
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException io)
		{
			io.printStackTrace();
		}
		finally{
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

	public void close() {

	}
}
